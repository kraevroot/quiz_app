Rails.application.routes.draw do
  namespace :admin do
      resources :users
      resources :answers
      resources :questions
      resources :technologies
      resources :themes
      resources :user_answers
      resources :user_answer_histories
      resources :user_technologies
      resources :user_themes

      root to: "users#index"
    end
  resources :technologies do
    resources :themes
  end
  devise_for :users
  get 'static_pages/home'
  get 'static_pages/about'

  resources :user_technologies, only: [:index, :show, :update, :destroy] do
    resources :user_themes, only: [:show] do
      resources :user_answers, only: [:new, :create, :destroy, :index]
    end
  end

  root 'static_pages#home'
end
