require "rails_helper"

RSpec.describe Technology, type: :model do
  context "with blank description" do
    it "valid" do
      technology = Technology.create(name: "Flutter framework", description: "")
      expect(technology).to be_valid
    end
  end
end
