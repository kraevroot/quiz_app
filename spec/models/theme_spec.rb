require "rails_helper"

RSpec.describe Theme, type: :model do
  context "nested attributes" do
    it "should accept nested attributes" do
      should accept_nested_attributes_for(:questions).allow_destroy(true)
    end
  end
end
