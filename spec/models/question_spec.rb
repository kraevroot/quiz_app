require "rails_helper"

RSpec.describe Question do
  context "nested attributes" do
    it "should accept nested attributes" do
      should accept_nested_attributes_for(:answers).allow_destroy(true)
    end
  end
end
