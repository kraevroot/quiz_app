class CreateUserAnswers < ActiveRecord::Migration[5.2]
  def change
    create_table :user_answers do |t|
      t.belongs_to :user_theme, index: true
      t.belongs_to :question, index: true
      t.belongs_to :answer, index: true

      t.timestamps
    end
  end
end
