class AddUserToUserAnswerHistories < ActiveRecord::Migration[5.2]
  def change
    add_reference :user_answer_histories, :user, foreign_key: true
  end
end
