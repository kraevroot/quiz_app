class FixColumnName < ActiveRecord::Migration[5.2]
  def change
    rename_column :answers, :corrent_answer, :correct_answer
  end
end
