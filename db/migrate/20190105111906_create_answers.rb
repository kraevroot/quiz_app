class CreateAnswers < ActiveRecord::Migration[5.2]
  def change
    create_table :answers do |t|
      t.belongs_to :question, index: true
      t.string :answer_text
      t.boolean :corrent_answer, default: false

      t.timestamps
    end
  end
end
