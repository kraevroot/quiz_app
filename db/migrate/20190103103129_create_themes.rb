class CreateThemes < ActiveRecord::Migration[5.2]
  def change
    create_table :themes do |t|
      t.string :name
      t.belongs_to :technology, index: true

      t.timestamps
    end
  end
end
