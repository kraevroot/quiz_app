require 'faker'

u1 = User.create(email: "test@test.test", password: "test@test.test", role: "admin")
u2 = User.create(email: "aaa@aaa.aaa", password: "aaa@aaa.aaa")

t1 = Technology.create(name: "Ruby on Rails", description: "MVC framework with a lot of magic!")
t2 = Technology.create(name: "Java", description: "Old OOP language!")

theme1 = Theme.create(name: "Model", technology_id: t1.id)
theme2 = Theme.create(name: "Controller", technology_id: t1.id)
theme3 = Theme.create(name: "Routes", technology_id: t1.id)


q1 = Question.create(question_text: Faker::Lorem.question, theme_id: theme1.id)
Answer.create(question_id: q1.id, answer_text: Faker::Lorem.word)
Answer.create(question_id: q1.id, answer_text: Faker::Lorem.word, correct_answer: true)
q2 = Question.create(question_text: Faker::Lorem.question, theme_id: theme1.id)
Answer.create(question_id: q2.id, answer_text: Faker::Lorem.word)
Answer.create(question_id: q2.id, answer_text: Faker::Lorem.word, correct_answer: true)
Answer.create(question_id: q2.id, answer_text: Faker::Lorem.word)
q3 = Question.create(question_text: Faker::Lorem.question, theme_id: theme2.id)
Answer.create(question_id: q3.id, answer_text: Faker::Lorem.word, correct_answer: true)
Answer.create(question_id: q3.id, answer_text: Faker::Lorem.word)

q4 = Question.create(question_text: Faker::Lorem.question, theme_id: theme3.id)
Answer.create(question_id: q4.id, answer_text: Faker::Lorem.word, correct_answer: true)
Answer.create(question_id: q4.id, answer_text: Faker::Lorem.word)
Answer.create(question_id: q4.id, answer_text: Faker::Lorem.word)
q5 = Question.create(question_text: Faker::Lorem.question, theme_id: theme3.id)
Answer.create(question_id: q5.id, answer_text: Faker::Lorem.word, correct_answer: true)
Answer.create(question_id: q5.id, answer_text: Faker::Lorem.word)
Answer.create(question_id: q5.id, answer_text: Faker::Lorem.word)


theme4 = Theme.create(name: "Classes", technology_id: t2.id)
q6 = Question.create(question_text: Faker::Lorem.question, theme_id: theme4.id)
Answer.create(question_id: q6.id, answer_text: Faker::Lorem.word)
Answer.create(question_id: q6.id, answer_text: Faker::Lorem.word, correct_answer: true)
q7 = Question.create(question_text: Faker::Lorem.question, theme_id: theme4.id)
Answer.create(question_id: q7.id, answer_text: Faker::Lorem.word)
Answer.create(question_id: q7.id, answer_text: Faker::Lorem.word, correct_answer: true)
theme5 = Theme.create(name: "Reserved keywords", technology_id: t2.id)
q8 = Question.create(question_text: Faker::Lorem.question, theme_id: theme5.id)
Answer.create(question_id: q8.id, answer_text: Faker::Lorem.word)
Answer.create(question_id: q8.id, answer_text: Faker::Lorem.word)
Answer.create(question_id: q8.id, answer_text: Faker::Lorem.word, correct_answer: true)
Answer.create(question_id: q8.id, answer_text: Faker::Lorem.word)
q9 = Question.create(question_text: Faker::Lorem.question, theme_id: theme5.id)
Answer.create(question_id: q9.id, answer_text: Faker::Lorem.word, correct_answer: true)
Answer.create(question_id: q9.id, answer_text: Faker::Lorem.word)
theme6 = Theme.create(name: "Interfaces", technology_id: t2.id)
q10 = Question.create(question_text: Faker::Lorem.question, theme_id: theme6.id)
Answer.create(question_id: q10.id, answer_text: Faker::Lorem.word)
Answer.create(question_id: q10.id, answer_text: Faker::Lorem.word, correct_answer: true)
q11 = Question.create(question_text: Faker::Lorem.question, theme_id: theme6.id)
Answer.create(question_id: q11.id, answer_text: Faker::Lorem.word)
Answer.create(question_id: q11.id, answer_text: Faker::Lorem.word, correct_answer: true)
Answer.create(question_id: q11.id, answer_text: Faker::Lorem.word)
Answer.create(question_id: q11.id, answer_text: Faker::Lorem.word)

10.times do
  tech = Technology.create(name: Faker::Lorem.sentence, description: Faker::Lorem.paragraph)
  8.times do
    theme = Theme.create(name: Faker::Lorem.word, technology_id: tech.id)
    5.times do
      question = Question.create(question_text: Faker::Lorem.question, theme_id: theme.id)
      Answer.create(question_id: question.id, answer_text: Faker::Lorem.word)
      Answer.create(question_id: question.id, answer_text: Faker::Lorem.word)
      Answer.create(question_id: question.id, answer_text: Faker::Lorem.word, correct_answer: true)
    end
  end
end
