document.addEventListener("turbolinks:load", function() {
    colorTable();
});

function colorTable() {
    let questions_with_results = document.getElementsByClassName("question_results");
    let user_correct_answers_count = 0;

    for (let question_answers of questions_with_results) {
        let qa_nodes = question_answers.childNodes;
        let question = qa_nodes[0].textContent;
        let user_answer = qa_nodes[1].textContent;
        let correct_answer = qa_nodes[2].textContent;
        if (user_answer == correct_answer) {
            question_answers.bgColor = "#B9FDB9"; /* green */
            user_correct_answers_count += 1;
        } else {
            question_answers.bgColor = "#FDB9B9"; /* red */
        }
    }

    let result_node = document.getElementById("result");
    let user_correct_answers_percent = (user_correct_answers_count / questions_with_results.length).toFixed(2) * 100;
    result_node.innerText = `Correct answers: ${user_correct_answers_percent}% (${user_correct_answers_count} / ${questions_with_results.length} )`;

}
