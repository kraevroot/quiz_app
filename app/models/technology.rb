class Technology < ApplicationRecord
  has_many :user_technologies
  has_many :users, through: :user_technologies

  has_many :themes
end
