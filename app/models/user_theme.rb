class UserTheme < ApplicationRecord
  belongs_to :user
  belongs_to :theme
  belongs_to :user_technology

  def name
    self.theme.name
  end
end
