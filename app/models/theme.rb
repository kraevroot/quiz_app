class Theme < ApplicationRecord
  belongs_to :technology

  has_many :questions, dependent: :destroy

  accepts_nested_attributes_for :questions, reject_if: lambda {|e| e[:question_text].blank?}, allow_destroy: true
end
