class Answer < ApplicationRecord
    belongs_to :question
    validates_inclusion_of :correct_answer, :in => [true, false]
end
