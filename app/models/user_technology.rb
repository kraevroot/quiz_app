class UserTechnology < ApplicationRecord
  belongs_to :user
  belongs_to :technology
  has_many :user_themes, dependent: :destroy

  def name
    self.technology.name
  end
end
