class User < ApplicationRecord
  enum role: [:user, :admin, :moderator]
  after_initialize :set_default_role, if: :new_record?

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :user_technologies
  has_many :technologies, through: :user_technologies

  def set_default_role
    self.role ||= :user
  end
end
