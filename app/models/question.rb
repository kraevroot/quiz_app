class Question < ApplicationRecord
    belongs_to :theme

    has_many :answers, dependent: :destroy
    accepts_nested_attributes_for :answers, reject_if: lambda {|e| e[:answer_text].blank?}, allow_destroy: true

    def user_answers(user)
      user_answers = UserAnswer.where(user_id: user.id, question_id: self.id)
      user_answers.map { |e| e.answer.answer_text }.join(",")
    end

    def user_answer_history(user)
      user_answers_history = UserAnswerHistory.where(user_id: user.id, question_id: self.id)
      user_answers_history.map { |e| e.answer.answer_text }.join(",")
    end

    def correct_answers
      answers = Answer.where(question_id: self.id, correct_answer: true)
      answers.map { |e| e.answer_text }.uniq.join(",")
    end
end
