require "administrate/base_dashboard"

class UserAnswerHistoryDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    question: Field::BelongsTo,
    answer: Field::BelongsTo,
    user_theme: Field::BelongsTo,
    id: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    user_id: Field::Number,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :question,
    :answer,
    :user_theme,
    :id,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :question,
    :answer,
    :user_theme,
    :id,
    :created_at,
    :updated_at,
    :user_id,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :question,
    :answer,
    :user_theme,
    :user_id,
  ].freeze

  # Overwrite this method to customize how user answer histories are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(user_answer_history)
  #   "UserAnswerHistory ##{user_answer_history.id}"
  # end
end
