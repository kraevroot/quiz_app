class ApplicationController < ActionController::Base
  add_flash_types :success, :warning, :danger, :info, :alert
  include Pundit
  protect_from_forgery

  rescue_from Pundit::NotAuthorizedError do
    redirect_to root_url, warning: 'You do not have access to this page'
  end
end
