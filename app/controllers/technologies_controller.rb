class TechnologiesController < ApplicationController
  before_action :authenticate_user!, except: [:index]
  def index
    @technologies = Technology.all
    authorize @technologies
  end

  def create
    @technology = Technology.new(technology_params)
    authorize @technology

    if @technology.save
      redirect_to action: :index, success: "Created successfully."
    else
      render action: :new
    end
  end

  def new
    @technology = Technology.new
    authorize @technology
  end

  def edit
    @technology = Technology.find(params[:id])
    authorize @technology
  end

  def show
    @technology = Technology.find(params[:id])
    authorize @technology
  end

  def update
    @technology = Technology.find(params[:id])
    authorize @technology

    if @technology.update_attributes(technology_params)
      redirect_to action: :show, id: @technology, success: "Updated successfully."
    else
      render action: :edit
    end
  end

  def destroy
    @technology = Technology.find(params[:id])
    authorize @technology
    UserTechnology.where(technology_id: params[:id]).destroy_all
    @technology.destroy
    redirect_to technologies_path, info: "Removed successfully."
  end

  private

  def technology_params
    params.require(:technology).permit(:name, :description)
  end
end
