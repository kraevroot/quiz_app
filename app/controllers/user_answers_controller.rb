class UserAnswersController < ApplicationController
  before_action :authenticate_user!
  def index
    @user_technology = UserTechnology.find(params[:user_technology_id])
    @user_theme = UserTheme.find(params[:user_theme_id])
    @questions = @user_theme.theme.questions
    @user_answers = UserAnswer.find_by(user_theme_id: @user_theme.id)
  end

  def new
    @user_technology = UserTechnology.find(params[:user_technology_id])
    @user_theme = UserTheme.find(params[:user_theme_id])
    @questions = @user_theme.theme.questions
    @user_answer = UserAnswer.new(user_theme_id: params[:user_theme_id])
  end

  def create
    @current_user = current_user
    @user_technology = UserTechnology.find(params[:user_technology_id])
    @user_theme = UserTheme.find(params[:user_theme_id])
    @questions = @user_theme.theme.questions

    params[:user_answer].each do |question_id, answer_ids|
      answers = answer_ids.reject { |e| e == "0" }
      user_answers = UserAnswer.where(user_theme_id: @user_theme.id, question_id: question_id, user_id: @current_user.id)

      UserAnswerHistory.where(user_theme_id: @user_theme.id, question_id: question_id, user_id: @current_user.id).in_batches do |user_answer_history_batch|
        user_answer_history_batch.delete_all
    end

      user_answers.each do |user_answer|
        UserAnswerHistory.create!(user_answer.attributes)
        user_answer.destroy
      end

      answers.each do |answer_id|
        @user_answer = UserAnswer.create!(user_theme_id: params[:user_theme_id], question_id: question_id, answer_id: answer_id, user_id: @current_user.id)
      end

    end
    render :index
  end
end
