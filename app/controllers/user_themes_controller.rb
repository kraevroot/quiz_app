class UserThemesController < ApplicationController
  before_action :authenticate_user!
  def show
    @user_technology = UserTechnology.find(params[:user_technology_id])
    @user_theme = UserTheme.find(params[:id])
    @questions = @user_theme.theme.questions
  end
end
