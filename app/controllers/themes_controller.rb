class ThemesController < ApplicationController
  def new
    @technology = Technology.find(params[:technology_id])
    @theme = Theme.new
    2.times do
        question = @theme.questions.build
        2.times { question.answers.build }
    end
  end

  def edit
    @technology = Technology.find(params[:technology_id])
    @theme = Theme.find(params[:id])
  end

  def update
    @technology = Technology.find(params[:technology_id])
    @theme = Theme.find(params[:id])

    if @theme.update_attributes(theme_params)
      render action: :show
    else
      render action: :edit
    end
  end

  def show
    @theme = Theme.find(params[:id])
  end

  def create
    @theme = Theme.new(theme_params)
    @theme.technology_id = params[:technology_id]
    @technology = Technology.find(params[:technology_id])

    if @theme.save
      redirect_to @technology
    else
      render action: :new
    end
  end

  def destroy
    @theme = Theme.find(params[:id])
    @technology = Technology.find(params[:technology_id])
    @user_themes = UserTheme.where(theme_id: params[:id]).destroy_all
    @theme.destroy

    redirect_to @technology
  end

  private

  def theme_params
    params.require(:theme).permit(:name, questions_attributes: [:id, :question_text, answers_attributes: [:answer_text, :correct_answer, :id]])
  end
end
