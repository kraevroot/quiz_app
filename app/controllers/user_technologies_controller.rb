class UserTechnologiesController < ApplicationController
  before_action :authenticate_user!
  def index
    @user = current_user
    @user_technologies = UserTechnology.where(user_id: @user.id)
  end

  def show
    @user = current_user
    @user_technology = UserTechnology.find_by(id: params[:id], user_id: @user.id)
  end

  def update
    # add handler for ":no_content"
    @user = current_user
    @technology = Technology.find(params[:id])
    @user_technology = UserTechnology.find_by(user_id: @user.id, technology_id: @technology.id)
    if @user_technology.nil?
      @user_technology = UserTechnology.create!(user_id: @user.id, technology_id: @technology.id)

      @technology.themes.each do |theme|
        UserTheme.create!(user_id: @user.id, theme_id: theme.id, user_technology_id: @user_technology.id)
      end
    end
  end

  def destroy
    @user = current_user
    @user_technology = UserTechnology.find_by(user_id: @user.id, id: params[:id])
    if @user_technology
      @user_technology.destroy
      redirect_to user_technologies_path, success: "Removed successfully."
    end
  end
end
