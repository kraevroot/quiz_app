module Admin
  class ApplicationController < Administrate::ApplicationController
    before_action :authenticate_admin

    def authenticate_admin
      redirect_to root_path, alert: "Not authorized." unless current_user&.admin?
    end

    def records_per_page
      params[:per_page] || 25
    end
  end
end
